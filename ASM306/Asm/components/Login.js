/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text, View,
  TextInput, TouchableOpacity, TouchableWithoutFeedback, Keyboard
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
  }

  render() {
    //thêm đường kẻ giữa 2 button: Divider
    const Divider = (props) => {
      return <View {...props}>
        <View style={styles.line}></View>
        <Text style={styles.textOR}>OR</Text>
        <View style={styles.line}></View>
      </View>
    }

    return (
      //bấm ra ngoài bàn phím tuột xuống: TouchableWithoutFeedback
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <View style={styles.up}>
            <FontAwesome name="android" size={150} color="#9933cc" />
            <Text style={styles.title}>REACT NATIVE</Text>

          </View>

          <View style={styles.down}>

            <View style={styles.textInputContainer}>

              <TextInput style={styles.textInput}
                onChangeText={(email) => this.setState({ email })}
                value={this.state.email} />

            </View>

            <View style={styles.textInputContainer}>

              <TextInput style={styles.textInput}
                secureTextEntry={true}
                onChangeText={(password) => this.setState({ password })}
                value={this.state.password} />

            </View>

            <TouchableOpacity style={styles.btnLogin}>
              <Text style={styles.btnLoginTitle}>LOGIN</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.btnLogin}>
              onPress={()=>{this.props.goRegister()}} 

              <Text style={styles.btnLoginTitle}>REGISTER</Text>
            </TouchableOpacity>

            <Divider style={styles.divider}></Divider>

            <FontAwesome.Button name="facebook"
              style={styles.btnFb}
              backgroundColor="#0033cc">
              <Text style={styles.btnLoginTitle}>Login with Facebook</Text>
            </FontAwesome.Button>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#6699ff'
  },

  up: {
    flex: 4,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },

  down: {
    flex: 6,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'

  },
  title: {
    color: 'white',
    textAlign: 'center',
    width: 400,
    fontSize: 25
  },

  textInputContainer: {
    paddingHorizontal: 10,
    borderRadius: 10,
    marginTop: 20,
    backgroundColor: '#9999ff' //a = alpha = độ trong
  },

  textInput: {
    width: 280,
    height: 45
  },

  btnLogin: {
    width: 300,
    height: 45,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9933cc',
    marginTop: 15
  },

  btnLoginTitle: {
    fontSize: 18,
    color: 'white'
  },

  btnFb: {
    width: 300,
    height: 45,
    borderRadius: 15,
    justifyContent: 'center',
  },

  line: {
    height: 1,
    flex: 2,
    backgroundColor: 'black'
  },

  textOR: {
    flex: 1,
    textAlign: 'center'
  },

  divider: {
    flexDirection: 'row',
    height: 40,
    width: 298,
    justifyContent: 'center',
    alignItems: 'center'
  }
})


