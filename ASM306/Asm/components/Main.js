import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Login from '../components/Login.js';
import Register from '../components/Register.js';

export default class Main extends Component{
    renderScene(route, navigator){
        switch(route.name) {
            case 'login':return(
                <Login
                goRegister={()=>{
                    navigator.push({name: 'register'})
                }}
                />
            );
            case 'register': return(
                <Register/>
            );
        }
    }
    render() {
        return(
            <Navigator
            initialRoute={{name:'login'}}
            renderScene={this.renderScene}/>
        );
    }
}