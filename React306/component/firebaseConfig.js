import * as firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyCxEfiDTUYFp_MauGFvI6wjL1tUGt9jywk",
    authDomain: "react306-35afe.firebaseapp.com",
    databaseURL: "https://react306-35afe.firebaseio.com",
    projectId: "react306-35afe",
    storageBucket: "react306-35afe.appspot.com",
    messagingSenderId: "352415186903",
    appId: "1:352415186903:web:4d9f6e2cff800848039592",
    measurementId: "G-3MT1ZYBENY"
  };
  // Initialize Firebase
 export const firebaseApp = firebase.initializeApp(firebaseConfig);